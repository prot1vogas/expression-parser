<?php

namespace Expression;

use Expression\Tokens\TokenInterface;

interface ExpressionInterface
{
    /**
     * @return TokenInterface[]
     */
    public function getTokens();
}