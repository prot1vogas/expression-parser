<?php

namespace Expression\Tokens;

/**
 * Interface TokenInterface
 * @package Expression\Tokens
 */
interface TokenInterface
{
    /**
     * @param array $arguments
     * @return mixed
     */
    public function getValue($arguments = []);

    /**
     * @return int
     */
    public function getArgumentsCount();

    /**
     * @return string
     */
    public function getTokenString();
}