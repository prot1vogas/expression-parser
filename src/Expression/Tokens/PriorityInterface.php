<?php

namespace Expression\Tokens;

/**
 * Interface PriorityInterface
 * @package Expression\Tokens
 */
interface PriorityInterface
{
    /**
     * @return int
     */
    public function getPriority();
}