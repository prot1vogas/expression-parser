<?php

namespace Expression\Tokens;

/**
 * Class Operation
 * @package Expression
 */
abstract class Operation extends TokenType implements TokenInterface, PriorityInterface
{
    /**
     * @var callable
     */
    private $operation;

    /**
     * @var int
     */
    private $priority;

    /**
     * @var string
     */
    private $name;

    /**
     * Operation constructor.
     * @param $name string
     * @param callable $operation
     * @param $priority int
     */
    public function __construct($name, callable $operation, $priority)
    {
        $this->operation = $operation;
        $this->priority = $priority;
        $this->name = $name;
    }

    /**
     * @param $arguments
     * @return mixed
     * @throws \Exception
     */
    public function getValue($arguments = [])
    {
        if (count($arguments) !== $this->getArgumentsCount()) {
            throw new \Exception("Wrong argument count");
        }
        return call_user_func($this->operation, $arguments);
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @return string
     */
    public function getRegex()
    {
        return preg_quote($this->name, '/');
    }

    /**
     * @return string
     */
    public function getTokenString()
    {
        return $this->name;
    }
}
