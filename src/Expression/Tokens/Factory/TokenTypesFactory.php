<?php

namespace Expression\Tokens\Factory;

use Expression\Tokens\BinaryOperation;
use Expression\Tokens\BracketsPair;
use Expression\Exceptions\EvaluationException;
use Expression\Tokens\UnaryOperation;
use Expression\Tokens\Variable;

/**
 * Class TokenTypesFactory
 * @package Expression\Tokens\Factory
 */
class TokenTypesFactory implements TokenTypesFactoryInterface
{
    /**
     * @return UnaryOperation[]
     */
    public function getUnaryOperations()
    {
        $operations = [];

        $operations[] = new UnaryOperation('-', function ($args) {
            return -1 * $args[0];
        }, 20, UnaryOperation::PREFIX_NOTATION);

        $operations[] = new UnaryOperation('sqrt', function ($args) {
            return sqrt($args[0]);
        }, 20, UnaryOperation::PREFIX_NOTATION);

        $operations[] = new UnaryOperation('!', function ($args) {
            return $this->factorial($args[0]);
        }, 20, UnaryOperation::POSTFIX_NOTATION);

        $operations[] = new UnaryOperation('sin', function ($args) {
            return round(sin($args[0]), 5);
        }, 10, UnaryOperation::PREFIX_NOTATION);

        $operations[] = new UnaryOperation('cos', function ($args) {
            return round(cos($args[0]), 5);
        }, 10, UnaryOperation::PREFIX_NOTATION);

        $operations[] = new UnaryOperation('tan', function ($args) {
            return round(tan($args[0]), 5);
        }, 10, UnaryOperation::PREFIX_NOTATION);

        $operations[] = new UnaryOperation('cot', function ($args) {
            return round(1 / tan($args[0]), 5);
        }, 10, UnaryOperation::PREFIX_NOTATION);

        return $operations;
    }

    /**
     * @return BinaryOperation[]
     */
    public function getBinaryOperations()
    {
        $operations = [];

        $operations[] = new BinaryOperation('+', function ($args) {
            return $args[0] + $args[1];
        }, 5, BinaryOperation::LEFT_ASSOCIATIVITY);

        $operations[] = new BinaryOperation('-', function ($args) {
            return $args[0] - $args[1];
        }, 5, BinaryOperation::LEFT_ASSOCIATIVITY);

        $operations[] = new BinaryOperation('*', function ($args) {
            return $args[0] * $args[1];
        }, 15, BinaryOperation::LEFT_ASSOCIATIVITY);

        $operations[] = new BinaryOperation('/', function ($args) {
            if ($args[1] == 0) {
                throw new EvaluationException("Division by zero");
            }
            return $args[0] / $args[1];
        }, 15, BinaryOperation::LEFT_ASSOCIATIVITY);

        $operations[] = new BinaryOperation('%', function ($args) {
            $divider = (int)$args[1];
            if ($divider == 0) {
                throw new EvaluationException("Division by zero");
            }
            return $args[0] % $args[1];
        }, 15, BinaryOperation::LEFT_ASSOCIATIVITY);

        $operations[] = new BinaryOperation('^', function ($args) {
            return pow($args[0], $args[1]);
        }, 20, BinaryOperation::RIGHT_ASSOCIATIVITY);

        return $operations;
    }

    /**
     * @return Variable[]
     */
    public function getVariables()
    {
        $variables = [];

        $variables[] = new Variable('\d+(?:\.\d+)?', function ($value) {
            return (float)$value;
        });

        $variables[] = new Variable('pi', function ($value) {
            return M_PI;
        });

        $variables[] = new Variable('e', function ($value) {
            return M_E;
        });

        return $variables;
    }

    /**
     * @return BracketsPair[]
     */
    public function getBrackets()
    {
        $brackets = [];
        $brackets[] = new BracketsPair('(', ')');
        $brackets[] = new BracketsPair('[', ']');
        return $brackets;
    }

    /**
     * @param $arg
     * @return int|mixed
     */
    private function factorial($arg)
    {
        $val = intval($arg);
        return ($val > 1) ? $val * $this->factorial($val - 1) : 1;
    }
}
