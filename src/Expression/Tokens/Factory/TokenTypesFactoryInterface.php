<?php

namespace Expression\Tokens\Factory;

/**
 * Interface TokenTypesFactoryInterface
 * @package Expression\Tokens\Factory
 */
interface TokenTypesFactoryInterface
{
    public function getUnaryOperations();
    public function getBinaryOperations();
    public function getVariables();
    public function getBrackets();
}