<?php

namespace Expression\Tokens;

/**
 * Class Bracket
 * @package Expression
 */
class Bracket extends TokenType implements PriorityInterface
{
    const TYPE_OPEN = 1;
    const TYPE_CLOSE = 2;
    /**
     * @var string
     */
    private $value;
    /**
     * @var int
     */
    private $type;

    /**
     * Bracket constructor.
     * @param $value
     * @param int $type
     */
    public function __construct($value, $type = self::TYPE_OPEN)
    {
        $this->value = $value;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getRegex()
    {
        return preg_quote($this->value, '/');
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return 0;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
}