<?php

namespace Expression\Tokens;

/**
 * Class BinaryOperation
 * @package Expression
 */
class BinaryOperation extends Operation
{
    const LEFT_ASSOCIATIVITY = 1;
    const RIGHT_ASSOCIATIVITY = 2;

    /**
     * @var int
     */
    private $associativity;

    /**
     * BinaryOperation constructor.
     * @param $name
     * @param callable $operation
     * @param $priority
     * @param int $associativity
     */
    public function __construct($name, callable $operation, $priority, $associativity = self::LEFT_ASSOCIATIVITY)
    {
        parent::__construct($name, $operation, $priority);
        $this->associativity = $associativity;
    }

    /**
     * @return int
     */
    public function getArgumentsCount()
    {
        return 2;
    }

    /**
     * @return int
     */
    public function getAssociativity()
    {
        return $this->associativity;
    }
}