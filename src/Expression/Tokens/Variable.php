<?php


namespace Expression\Tokens;

/**
 * Class Variable
 * @package Expression
 */
class Variable extends TokenType
{
    /**
     * @var string
     */
    private $regex;
    /**
     * @var callable
     */
    private $getValueFunc;

    public function __construct($regex, callable $getValueFunc)
    {
        $this->regex = $regex;
        $this->getValueFunc = $getValueFunc;
    }

    public function getValue($value)
    {
        $callable = $this->getValueFunc;
        return $callable($value);
    }

    public function getRegex()
    {
        return $this->regex;
    }
}
