<?php


namespace Expression\Tokens;

/**
 * Class UnaryOperation
 * @package Expression
 */
class UnaryOperation extends Operation
{
    const PREFIX_NOTATION = 1;
    const POSTFIX_NOTATION = 2;

    /**
     * @var int
     */
    private $notation;

    /**
     * UnaryOperation constructor.
     * @param $name
     * @param callable $operation
     * @param $priority
     * @param int $notation
     */
    public function __construct($name, callable $operation, $priority, $notation = self::PREFIX_NOTATION)
    {
        parent::__construct($name, $operation, $priority);
        $this->notation = $notation;
    }

    /**
     * @return int
     */
    public function getArgumentsCount()
    {
        return 1;
    }

    /**
     * @return int
     */
    public function getNotation()
    {
        return $this->notation;
    }
}