<?php


namespace Expression\Tokens;

/**
 * Class TokenType
 * @package Expression\Tokens
 */
abstract class TokenType
{
    /**
     * @return string
     */
    abstract public function getRegex();
}