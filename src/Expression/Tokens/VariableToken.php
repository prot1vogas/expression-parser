<?php

namespace Expression\Tokens;

/**
 * Class VariableToken
 * @package Expression\Tokens
 */
class VariableToken implements TokenInterface
{
    /**
     * @var string
     */
    private $value;
    /**
     * @var Variable
     */
    private $variable;

    /**
     * VariableToken constructor.
     * @param $value
     * @param Variable $variable
     */
    public function __construct($value, Variable $variable)
    {
        $this->value = $value;
        $this->variable = $variable;
    }

    /**
     * @param array $arguments
     * @return float
     * @throws \Exception
     */
    public function getValue($arguments = [])
    {
        if (count($arguments) !== $this->getArgumentsCount()) {
            throw new \Exception("Wrong argument count");
        }
        return $this->variable->getValue($this->value);
    }

    /**
     * @return int
     */
    public function getArgumentsCount()
    {
        return 0;
    }

    /**
     * @return string
     */
    public function getTokenString()
    {
        return $this->value;
    }
}