<?php

namespace Expression\Tokens;

/**
 * Class BracketsPair
 * @package Expression
 */
class BracketsPair extends TokenType
{
    /**
     * @var Bracket
     */
    private $open;
    /**
     * @var Bracket
     */
    private $close;

    /**
     * BracketsPair constructor.
     * @param $open
     * @param $close
     */
    public function __construct($open, $close)
    {
        $this->open = new Bracket($open, Bracket::TYPE_OPEN);
        $this->close = new Bracket($close, Bracket::TYPE_CLOSE);
    }

    /**
     * @return string
     */
    public function getRegex()
    {
        return $this->open->getRegex() . '|' . $this->close->getRegex();
    }

    /**
     * @param $type
     * @return Bracket
     */
    public function getBracket($type)
    {
        return $type == Bracket::TYPE_OPEN ? $this->open : $this->close;
    }
}