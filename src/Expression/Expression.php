<?php


namespace Expression;

use Expression\Tokens\TokenInterface;

/**
 * Class Expression
 * @package Expression
 */
class Expression implements ExpressionInterface
{
    /**
     * @var TokenInterface[]
     */
    private $tokens;

    /**
     * Expression constructor.
     * @param $tokens TokenInterface[]
     */
    public function __construct($tokens)
    {
        $this->tokens = $tokens;
    }

    /**
     * @return TokenInterface[]
     */
    public function getTokens()
    {
        return $this->tokens;
    }
}