<?php


namespace Expression;

use Expression\Exceptions\EvaluationException;

/**
 * Class Evaluator
 * @package Expression
 */
class Evaluator implements EvaluatorInterface
{
    /**
     * @param ExpressionInterface $expression
     * @return mixed
     * @throws EvaluationException
     */
    public function execute(ExpressionInterface $expression) {

        $stack = new \SplStack();

        foreach ($expression->getTokens() as $token) {
            $arguments = $this->getArgumentValues($stack, $token->getArgumentsCount());
            $stack->push($token->getValue($arguments));
        }
        if ($stack->count() != 1) {
            throw new EvaluationException('Evaluation Error');
        }
        return $stack->pop();
    }

    /**
     * @param \SplStack $stack
     * @param $count
     * @return array
     * @throws EvaluationException
     */
    private function getArgumentValues(\SplStack $stack, $count)
    {
        $values = [];
        for ($i = $count - 1; $i >= 0; $i--) {
            if ($stack->isEmpty()) {
                throw new EvaluationException('Evaluation Error');
            }
            $values[$i] = $stack->pop();
        }

        return $values;
    }
}