<?php


namespace Expression;

use Expression\Lexer\Lexeme;
use Expression\Exceptions\ParserException;
use Expression\Tokens\BinaryOperation;
use Expression\Tokens\Bracket;
use Expression\Tokens\BracketsPair;
use Expression\Lexer\LexerInterface;
use Expression\Tokens\Factory\TokenTypesFactoryInterface;
use Expression\Tokens\PriorityInterface;
use Expression\Tokens\TokenType;
use Expression\Tokens\UnaryOperation;
use Expression\Tokens\Variable;
use Expression\Tokens\VariableToken;

/**
 * Class Parser
 * @package Expression
 */
class Parser implements ParserInterface
{
    const STATE_BEFORE_VALUE = 0;
    const STATE_AFTER_VALUE = 1;

    /**
     * @var LexerInterface
     */
    private $lexer;

    /**
     * @var BinaryOperation[]
     */
    private $binaryOperations;

    /**
     * @var UnaryOperation[]
     */
    private $unaryOperations;

    /**
     * @var Variable[]
     */
    private $variables;

    /**
     * @var BracketsPair[]
     */
    private $bracketPairs;

    /**
     * ExpressionParser constructor.
     * @param TokenTypesFactoryInterface $factory
     * @param LexerInterface $lexer
     */
    public function __construct(TokenTypesFactoryInterface $factory, LexerInterface $lexer)
    {
        $this->binaryOperations = $factory->getBinaryOperations();
        $this->unaryOperations = $factory->getUnaryOperations();
        $this->variables = $factory->getVariables();
        $this->bracketPairs = $factory->getBrackets();
        $this->lexer = $lexer;
    }

    /**
     * @param string $expressionString
     * @return Expression
     * @throws ParserException
     */
    public function parse($expressionString)
    {
        $lexemes = $this->lexer->tokenize($expressionString, array_merge(
            $this->binaryOperations,
            $this->unaryOperations,
            $this->variables,
            $this->bracketPairs
        ));

        $state = self::STATE_BEFORE_VALUE;

        $output = [];
        $stack = new \SplStack();

        foreach ($lexemes as $lexeme) {
            if ($state == self::STATE_BEFORE_VALUE) {
                // Expected variable, prefix operation or open bracket
                if ($variable = $this->getVariable($lexeme)) {

                    // Add variable to output
                    $output[] = $variable;
                    $state = self::STATE_AFTER_VALUE;
                } elseif ($operation = $this->getUnaryOperation($lexeme, UnaryOperation::PREFIX_NOTATION)) {

                    // Push prefix operator to stack
                    $stack->push($operation);
                } elseif ($bracket = $this->getBracket($lexeme, Bracket::TYPE_OPEN)) {
                    // Push prefix open bracket to stack
                    $stack->push($bracket);
                } else {

                    // Error otherwise
                    throw new ParserException('Expected variable, prefix operation or opening bracket, got ' . $lexeme->getValue());
                }
            } elseif ($state == self::STATE_AFTER_VALUE) {
                // Expected binary operation, postfix operation or close bracket
                if ($operation = $this->getBinaryOperation($lexeme)) {

                    // Pop operations from stack to output, while priority of new operation higher than priority of operation from top of stack
                    while (!$stack->isEmpty() && $this->checkPriority($operation, $stack->top())) {
                        $output[] = $stack->pop();
                    }
                    $stack->push($operation);
                    $state = self::STATE_BEFORE_VALUE;
                } elseif ($operation = $this->getUnaryOperation($lexeme, UnaryOperation::POSTFIX_NOTATION)) {

                    // Add postfix operation to output
                    $output[] = $operation;
                } elseif ($closeBracket = $this->getBracket($lexeme, Bracket::TYPE_CLOSE)) {

                    // Pop all operations into while stack top is not bracket
                    // If last open bracket is not new bracket's pair - error
                    // If there is no open bracket in stack - error
                    while (!$stack->isEmpty() && !($stack->top() instanceof Bracket)) {
                        $output[] = $stack->pop();
                    }
                    if ($stack->isEmpty()) {
                        throw new ParserException('Unexpected closing bracket ' . $lexeme->getValue());
                    }
                    $openBracket = $stack->pop();
                    if (!$this->checkBracketPair($openBracket, $closeBracket)) {
                        throw new ParserException('Unexpected closing bracket ' . $lexeme->getValue());
                    }
                } else {

                    // Error otherwise
                    throw new ParserException('Expected binary operation, postfix operation or closing bracket, got ' . $lexeme->getValue());
                }
            }
        }

        if ($state == self::STATE_BEFORE_VALUE) {
            throw new ParserException('Unexpected end of expression');
        }

        while (!$stack->isEmpty()) {
            $token = $stack->pop();
            if ($token instanceof Bracket) {
                throw new ParserException('Missing closing bracket');
            }
            $output[] = $token;
        }
        return new Expression($output);
    }

    /**
     * @param Lexeme $lexeme
     * @param int $notation
     * @return UnaryOperation|bool
     */
    private function getUnaryOperation(Lexeme $lexeme, $notation = UnaryOperation::PREFIX_NOTATION)
    {
        foreach ($this->unaryOperations as $unaryOperation) {
            if ($unaryOperation->getNotation() == $notation && $this->checkRegex($lexeme, $unaryOperation)) {
                return $unaryOperation;
            }
        }
        return false;
    }

    /**
     * @param Lexeme $lexeme
     * @return BinaryOperation|bool
     */
    private function getBinaryOperation(Lexeme $lexeme)
    {
        foreach ($this->binaryOperations as $binaryOperation) {
            if ($this->checkRegex($lexeme, $binaryOperation)) {
                return $binaryOperation;
            }
        }
        return false;
    }

    /**
     * @param Lexeme $lexeme
     * @return VariableToken|bool
     */
    private function getVariable(Lexeme $lexeme)
    {
        foreach ($this->variables as $variable) {
            if ($this->checkRegex($lexeme, $variable)) {
                return new VariableToken($lexeme->getValue(), $variable);
            }
        }
        return false;
    }

    /**
     * @param Lexeme $lexeme
     * @param $type
     * @return Bracket|bool
     */
    private function getBracket(Lexeme $lexeme, $type)
    {
        foreach ($this->bracketPairs as $bracketPair) {
            if ($this->checkRegex($lexeme, $bracketPair->getBracket($type))) {
                return $bracketPair->getBracket($type);
            }
        }
        return false;
    }

    /**
     * @param Bracket $open
     * @param Bracket $close
     * @return bool
     */
    private function checkBracketPair(Bracket $open, Bracket $close)
    {
        foreach ($this->bracketPairs as $bracketPair) {
            if (
                $bracketPair->getBracket(Bracket::TYPE_OPEN)->getValue() == $open->getValue()
                && $bracketPair->getBracket(Bracket::TYPE_CLOSE)->getValue() == $close->getValue()
            ) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param Lexeme $lexeme
     * @param TokenType $tokenType
     * @return false|int
     */
    private function checkRegex(Lexeme $lexeme, TokenType $tokenType)
    {
        return preg_match("/^{$tokenType->getRegex()}$/i", $lexeme->getValue());
    }

    /**
     * @param BinaryOperation $operation
     * @param PriorityInterface $token
     * @return bool
     */
    private function checkPriority(BinaryOperation $operation, PriorityInterface $token)
    {
        if ($operation->getAssociativity() == BinaryOperation::LEFT_ASSOCIATIVITY) {
            return $operation->getPriority() <= $token->getPriority();
        } else {
            return $operation->getPriority() < $token->getPriority();
        }
    }
}
