<?php

namespace Expression;

use Expression\Exceptions\ParserException;

interface ParserInterface
{
    /**
     * @param string $expressionString
     * @return Expression
     * @throws ParserException
     */
    public function parse($expressionString);
}