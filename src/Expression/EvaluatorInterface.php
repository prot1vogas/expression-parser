<?php

namespace Expression;

use Expression\Exceptions\EvaluationException;

interface EvaluatorInterface
{
    /**
     * @param ExpressionInterface $expression
     * @return mixed
     * @throws EvaluationException
     */
    public function execute(ExpressionInterface $expression);
}