<?php


namespace Expression\Exceptions;

/**
 * Class ParserException
 * @package Expression\
 */
class ParserException extends \Exception
{
}