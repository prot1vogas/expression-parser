<?php

namespace Expression\Lexer;

use \Expression\Tokens\TokenType;

/**
 * Class Lexer
 * @package Expression
 */
class Lexer implements LexerInterface
{

    /**
     * @param $expressionString string
     * @param $tokenTypes TokenType[]
     * @return Lexeme[]
     */
    public function tokenize($expressionString, $tokenTypes) {
        $regex = $this->getRegex($tokenTypes);

        preg_match_all($regex, $expressionString, $matches, PREG_SET_ORDER);

        $lexemes = array_map(function($match) {
            return new Lexeme($match[0]);
        }, $matches);

        return $lexemes;
    }

    /**
     * @return string
     */
    private function getRegex($tokenTypes)
    {
        $sortedTokenTypes = $this->sortTokenTypes($tokenTypes);

        $regexes = array_map(function(TokenType $tokenType) {
            return $tokenType->getRegex();
        }, $sortedTokenTypes);

        $regexes[] = '(\S+)';

        return '/(?:' . implode('|', $regexes) . ')/i';
    }


    /**
     * @param $tokenTypes TokenType[]
     * @return TokenType[]
     */
    private function sortTokenTypes($tokenTypes)
    {
        usort($tokenTypes, function(TokenType $token1, TokenType $token2) {
            return strlen($token2->getRegex()) - strlen($token1->getRegex());
        });

        return $tokenTypes;
    }
}
