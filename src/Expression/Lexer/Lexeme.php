<?php

namespace Expression\Lexer;

/**
 * Class Lexeme
 * @package Expression
 */
class Lexeme
{
    private $value;

    /**
     * Lexeme constructor.
     * @param $string
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }
}