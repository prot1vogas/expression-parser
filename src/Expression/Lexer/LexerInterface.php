<?php

namespace Expression\Lexer;

use \Expression\Tokens\TokenType;

/**
 * Interface LexerInterface
 * @package Expression
 */
interface LexerInterface
{

    /**
     * @param $expressionString string
     * @param $tokenTypes TokenType[]
     * @return Lexeme[]
     */
    public function tokenize($expressionString, $tokenTypes);
}
