# Expression parser

[![pipeline status](https://gitlab.com/prot1vogas/expression-parser/badges/master/pipeline.svg)](https://gitlab.com/prot1vogas/expression-parser/commits/master)

Simple expression parser.

Compatible with PHP 5.4 and up.

## Basic usage

```php
$parser = new Parser(new TokenTypesFactory(), new Lexer());
$evaluator = new Evaluator();
$expression = $parser->parse("2 + 2");
echo $evaluator->execute($expression);
```

## Features

*  binary operations + - * / % ^
*  unary operations - sqrt ! sin cos tan
*  brackets
*  float variables
*  constants pi, e

You can override TokenTypesFactory to add custom operations, variables or brackets. There are some classes of token types:
*  BinaryOperation
*  UnaryOperation
*  Variable
*  Brackets

For example, new operation 'func' may be added in method `getUnaryOperations` like this:
```php
$operations[] = new UnaryOperation('func', function ($args) {
    return $args[0] / 2;
}, 15, UnaryOperation::PREFIX_NOTATION);
```