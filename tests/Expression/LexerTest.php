<?php

use PHPUnit\Framework\TestCase;
use Expression\Lexer\Lexer;
use Expression\Lexer\Lexeme;
use Expression\Tokens\Factory\TokenTypesFactory;
use Expression\Tokens\TokenType;

class LexerTest extends TestCase
{
    /**
     * @var Lexer
     */
    private $lexer;
    /**
     * @var TokenType[]
     */
    private $tokenTypes;

    protected function setUp()
    {
        $this->lexer = new Lexer();
        $factory = new TokenTypesFactory();
        $this->tokenTypes = array_merge(
            $factory->getUnaryOperations(),
            $factory->getVariables(),
            $factory->getBinaryOperations(),
            $factory->getBrackets()
        );
    }

    public function testLexer()
    {
        $lexemes1 = $this->lexer->tokenize("1 + 2", $this->tokenTypes);
        $this->assertEquals("1 + 2", $this->getLexemesString($lexemes1));

        $lexemes2 = $this->lexer->tokenize("", $this->tokenTypes);
        $this->assertEquals("", $this->getLexemesString($lexemes2));

        $lexemes3 = $this->lexer->tokenize("   1 +(    2*5)", $this->tokenTypes);
        $this->assertEquals("1 + ( 2 * 5 )", $this->getLexemesString($lexemes3));
    }

    public function getLexemesString($lexemes)
    {
        return implode( ' ', array_map(function(Lexeme $lexeme) {
            return $lexeme->getValue();
        }, $lexemes));
    }
}