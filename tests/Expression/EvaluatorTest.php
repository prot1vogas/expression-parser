<?php

use PHPUnit\Framework\TestCase;
use Expression\Evaluator;
use Expression\Expression;
use Expression\Exceptions\EvaluationException;
use Expression\Tokens\BinaryOperation;
use Expression\Tokens\Variable;
use Expression\Tokens\VariableToken;

class EvaluatorTest extends TestCase
{
    /**
     * @var Evaluator
     */
    private $evaluator;

    protected function setUp()
    {
        $this->evaluator = new Evaluator();
    }

    public function testEvaluator()
    {
        $tokens = [
            new VariableToken("10", new Variable('10', function ($value) {return (float)$value;})),
            new VariableToken("5", new Variable('5',  function ($value) {return (float)$value;})),
            new BinaryOperation('/', function($args){
                return $args[0] / $args[1];
            }, 5)
        ];

        $expression =  new Expression($tokens);

        $this->assertEquals(2, $this->evaluator->execute($expression));
    }

    public function testEmptyExpressionEvaluator()
    {
        $this->setExpectedException(get_class(new EvaluationException()));

        $expression =  new Expression([]);
        $this->evaluator->execute($expression);
    }
}