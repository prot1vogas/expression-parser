<?php

use Expression\Exceptions\ParserException;
use PHPUnit\Framework\TestCase;
use Expression\Lexer\Lexer;
use Expression\Parser;
use Expression\Tokens\TokenInterface;
use Expression\Tokens\Factory\TokenTypesFactory;
use Expression\Expression;

class ParserTest extends TestCase
{
    /**
     * @var Parser
     */
    private $parser;

    protected function setUp()
    {
        $this->parser = new Parser(new TokenTypesFactory(), new Lexer());
    }

    public function testBinaryOperationsParsing()
    {
        $expression1 = $this->parser->parse("2+2");
        $this->assertEquals("2 2 +", $this->getExpressionString($expression1));

        $expression2 = $this->parser->parse("2-2");
        $this->assertEquals("2 2 -", $this->getExpressionString($expression2));

        $expression3 = $this->parser->parse("2/2");
        $this->assertEquals("2 2 /", $this->getExpressionString($expression3));

        $expression4 = $this->parser->parse("2%2");
        $this->assertEquals("2 2 %", $this->getExpressionString($expression4));

        $expression5 = $this->parser->parse("2*2");
        $this->assertEquals("2 2 *", $this->getExpressionString($expression5));
    }

    public function testUnaryOperationsParsing()
    {
        $expression1 = $this->parser->parse("-2");
        $this->assertEquals("2 -", $this->getExpressionString($expression1));

        $expression2 = $this->parser->parse("sqrt 2");
        $this->assertEquals("2 sqrt", $this->getExpressionString($expression2));

        $expression3 = $this->parser->parse("sin pi");
        $this->assertEquals("pi sin", $this->getExpressionString($expression3));

        $expression4 = $this->parser->parse("cos pi");
        $this->assertEquals("pi cos", $this->getExpressionString($expression4));

        $expression5 = $this->parser->parse("tan pi");
        $this->assertEquals("pi tan", $this->getExpressionString($expression5));

        $expression6 = $this->parser->parse("cot pi");
        $this->assertEquals("pi cot", $this->getExpressionString($expression6));
    }

    public function testBracketsParsing()
    {
        $expression1 = $this->parser->parse("(2)");
        $this->assertEquals("2", $this->getExpressionString($expression1));

        $expression2 = $this->parser->parse("(5 + 5) * 2");
        $this->assertEquals("5 5 + 2 *", $this->getExpressionString($expression2));

        $expression3 = $this->parser->parse("sqrt (2 + 2)");
        $this->assertEquals("2 2 + sqrt", $this->getExpressionString($expression3));

        $expression4 = $this->parser->parse("(2 + [3 - 1]) * 4");
        $this->assertEquals("2 3 1 - + 4 *", $this->getExpressionString($expression4));
    }

    public function testOperationsPriorityParsing()
    {
        $expression1 = $this->parser->parse("1 + 2 * 3");
        $this->assertEquals("1 2 3 * +", $this->getExpressionString($expression1));

        $expression2 = $this->parser->parse("20 / 10 / 2");
        $this->assertEquals("20 10 / 2 /", $this->getExpressionString($expression2));

        $expression3 = $this->parser->parse("e ^ 2 ^ 1");
        $this->assertEquals("e 2 1 ^ ^", $this->getExpressionString($expression3));

        $expression4 = $this->parser->parse("cos pi/2");
        $this->assertEquals("pi 2 / cos", $this->getExpressionString($expression4));
    }

    public function testException()
    {
        $this->setExpectedException(get_class(new ParserException()));
        $expression1 = $this->parser->parse("1 + 2 *");
        $this->getExpressionString($expression1);
    }

    /**
     * @param Expression $expression
     * @return string
     */
    private function getExpressionString(Expression $expression)
    {
        return implode( ' ', array_map(function(TokenInterface $token) {
            return $token->getTokenString();
        }, $expression->getTokens()));
    }
}